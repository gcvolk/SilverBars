package com.liveorderboard.exceptions;


/**
 * @author Gil
 *
 */
public class OrderNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public OrderNotFoundException() {
		super(ErrorMessage.Error.ORDER_NOT_FOUND.toString());
	}
}
