package com.liveorderboard.exceptions;

/**
 * @author 
 *
 */
public class ErrorMessage {
	
	 /**
	 * To print Error String
	 * @author Gil
	 *
	 */
	public enum Error{
	        ORDER_NOT_FOUND("Order not found");

	        private String errorDetails;

	        Error(String errorDetails){
	            this.errorDetails = errorDetails;
	        }

	        @Override
	        public String toString() {
	            return errorDetails;
	        }
	    }

}
