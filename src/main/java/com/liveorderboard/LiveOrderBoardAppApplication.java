package com.liveorderboard;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.liveorderboard.helper.OrderHelper;
import com.liveorderboard.model.Order;

/**
 * 
 * To display Live Order Board
 * Input Order entry is hard code in this class.
 * Since UI/WEB is not required here So commenting #SpringApplication.run# for further use.This application can be Simple Java Application as well instead of Spring boot app. 
 * 
 * @author Gil
 *
 */
@SpringBootApplication
public class LiveOrderBoardAppApplication {
	
	
	private static OrderHelper helper;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
	//	SpringApplication.run(LiveOrderBoardAppApplication.class, args);
	
		System.out.println("Live Order Board");
		
		    Order order1 = new Order("user1", 3.5, 306.0, Order.OrderType.SELL);
	       
	        Order order2 = new Order("user2", 1.2, 310.0, Order.OrderType.SELL);
	        
	        Order order3 = new Order("user3", 1.5, 307.0, Order.OrderType.SELL);
	       
	        Order order4 = new Order("user4", 2.0, 306.0, Order.OrderType.SELL);
	        
	        helper =new OrderHelper();
	        helper.registerOrder(order1);
	        helper.registerOrder(order2);
	        helper.registerOrder(order3);
	        helper.registerOrder(order4);
	  
		for (int i = 0; i <helper.orderList.size(); i++) {
			System.out.println(helper.getLiveOrder(Order.OrderType.SELL).get(i).getOrderQuantity()+"Kg for "+"\u00A3"+helper.getLiveOrder(Order.OrderType.SELL).get(i).getPrice()+" "
					+ "//"+helper.getLiveOrder(Order.OrderType.SELL).get(i).getUserId());	
		}
		
	}
	
	


	
}
