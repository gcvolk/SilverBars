package com.liveorderboard.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

import com.liveorderboard.model.Order;

/**
 * This is the class which is helper for register and showing live order.
 * @author Gil
 *
 */
public class OrderHelper {

	public List<Order> orderList = new ArrayList<>();

	/**
	 * @param order
	 * @return available order list for live Board.
	 */
	/**
	 * @param order
	 * @return
	 */
	public List<Order> registerOrder(Order order){
		ListIterator<Order> iterator = orderList.listIterator();
		Boolean samePFound = false;
		while(iterator.hasNext()){
			Order entryOrder = iterator.next();
			if(entryOrder.getPrice().equals(order.getPrice())){
				samePFound = true;
				entryOrder.setOrderQuantity(entryOrder.getOrderQuantity()+order.getOrderQuantity());
				entryOrder.setUserId(entryOrder.getUserId()+"+"+order.getUserId());
			}
		}
		if(!samePFound){
			orderList.add(order);
		}
		return orderList;
	}


	
	/**
	 * @param orderType like SELL and BUY
	 * @return Sorted Order list.
	 */
	public List<Order> getLiveOrder(Order.OrderType orderType){
		if(orderType.equals(Order.OrderType.SELL) ){
			Collections.sort(orderList, (Order o1, Order o2) -> o2.getOrderQuantity().compareTo(o1.getOrderQuantity()));
		}else {
			Collections.sort(orderList, (Order o1, Order o2) -> o1.getPrice().compareTo(o2.getPrice()));
		}
		return orderList;
	}


}
