package com.liveorderboard;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.liveorderboard.helper.OrderHelper;
import com.liveorderboard.model.Order;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author Gil
 *
 */
@RunWith(Parameterized.class)
public class OrderHelperTest {

    private String expectedWeight;
    private String expectedPrice;
    private String orderIndex;

    private OrderHelper orderHelper;

    @Before
    public void setUp() {
    	orderHelper = new OrderHelper();

        Order order1 = new Order("User1", 3.5, 306.0, Order.OrderType.SELL);
        orderHelper.registerOrder(order1);
        Order order2 = new Order("User2", 1.2, 310.0, Order.OrderType.SELL);
        orderHelper.registerOrder(order2);
        Order order3 = new Order("User3", 1.5, 307.0, Order.OrderType.SELL);
        orderHelper.registerOrder(order3);
        Order order4 = new Order("User4", 2.0, 306.0, Order.OrderType.SELL);
        orderHelper.registerOrder(order4);
        
        
    }

    public OrderHelperTest(String expectedWeight, String expectedPrice, String orderIndex) {
        this.expectedWeight = expectedWeight;
        this.expectedPrice = expectedPrice;
        this.orderIndex = orderIndex;
        
    }

    @Parameterized.Parameters
    public static Collection<String[]> addedNumbers() {
        return Arrays.asList(new String[][]{
                {"5.5", "306", "0"},
                {"1.5", "307", "1"},
                {"1.2", "310", "2"}
        });
    }

    /**
     * This is the method to checks equality for expected and calculated.  
     */
    @Test
    public void testSellOrder() {
        Assert.assertEquals(Double.valueOf(expectedWeight), orderHelper.getLiveOrder(Order.OrderType.SELL).get(Integer.valueOf(orderIndex)).getOrderQuantity());
        Assert.assertEquals(Double.valueOf(expectedPrice), orderHelper.getLiveOrder(Order.OrderType.SELL).get(Integer.valueOf(orderIndex)).getPrice());
    }
}